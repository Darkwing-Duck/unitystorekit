using System;
using System.Collections.Generic;

namespace UnityStoreKit
{
	public interface IStoreKitDelegate
	{
		void OnStoreKitWarning(String message);
		void OnPendingTransactionsReceived(List<USKPaymentTransaction> transactions);

		void OnProductsValidationSuccess(List<USKProduct> validProducts);
		void OnProductsValidationFail(String error);

		void OnProductPaymentSuccess(USKPaymentTransaction transaction);
		void OnProductPaymentFail(USKPaymentTransaction transaction);
	}
}
