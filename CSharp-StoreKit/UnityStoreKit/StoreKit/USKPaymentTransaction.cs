using System;
using UnityEngine;

namespace UnityStoreKit
{
	public enum USKPaymentTransactionState
	{
		Purchasing = 0,
		Purchased,
		Failed,
		Restored,
		Deferred
	}

	public class USKPaymentTransaction
	{
		public String TransactionIdentifier { get; protected set; }
		public String Error { get; protected set; }
		public USKPaymentTransactionState TransactionState { get; protected set; }
		public String TransactionReceipt { get; protected set; }
		public USKPaymentTransaction OriginalTransaction { get; protected set; }
		public USKPayment Payment { get; protected set; }

		public void UpdateFromObject(System.Object data)
		{
			System.Collections.IDictionary keyValueData = (System.Collections.IDictionary)data;

			TransactionIdentifier = keyValueData["transactionIdentifier"].ToString();
			TransactionState = (USKPaymentTransactionState)Int32.Parse(keyValueData["transactionState"].ToString());

			if (keyValueData.Contains("error"))
			{
				Error = keyValueData["error"].ToString();
			}

			if (keyValueData.Contains("originalTransaction"))
			{
				OriginalTransaction = new USKPaymentTransaction();
				OriginalTransaction.UpdateFromObject(keyValueData["originalTransaction"]);
			}

			if (keyValueData.Contains("transactionReceipt"))
			{
				TransactionReceipt = keyValueData["transactionReceipt"].ToString();
			}

			Payment = new USKPayment();
			Payment.UpdateFromObject(keyValueData["payment"]);
		}
	}
}