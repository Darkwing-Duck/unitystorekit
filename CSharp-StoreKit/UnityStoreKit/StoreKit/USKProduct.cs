using System;
using System.Collections.Generic;

namespace UnityStoreKit
{
	public class USKProduct
	{
		public String ProductIdentifier { get; protected set; }
		public String LocalizedTitle { get; protected set; }
		public String LocalizedDescription { get; protected set; }
		public Single Price { get; protected set; }
		public String FormattedPrice { get; protected set; }
		public Boolean IsDownloadable { get; protected set; }
		public String CurrencyCode { get; protected set; }
		public String CurrencySymbol { get; protected set; }
		public String LanguageCode { get; protected set; }
		public String CountryCode { get; protected set; }
		public String LocaleIdentifier { get; protected set; }

		public void UpdateFromObject(System.Object data)
		{
			System.Collections.IDictionary keyValueData = (System.Collections.IDictionary)data;

			ProductIdentifier = keyValueData["productIdentifier"].ToString();
			LocalizedTitle = keyValueData["localizedTitle"].ToString();
			LocalizedDescription = keyValueData["localizedDescription"].ToString();
			Price = Single.Parse(keyValueData["price"].ToString());
			IsDownloadable = Int32.Parse(keyValueData["isDownloadable"].ToString()) == 1;
			FormattedPrice = keyValueData["formattedPrice"].ToString();
			CurrencyCode = keyValueData["currencyCode"].ToString();
			CurrencySymbol = keyValueData["currencySymbol"].ToString();
			LanguageCode = keyValueData["languageCode"].ToString();
			CountryCode = keyValueData["countryCode"].ToString();
			LocaleIdentifier = keyValueData["localeIdentifier"].ToString();
		}
	}
}