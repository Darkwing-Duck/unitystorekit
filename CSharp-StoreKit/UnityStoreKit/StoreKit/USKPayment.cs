using System;

namespace UnityStoreKit
{
	public class USKPayment
	{
		public String ProductIdentifier { get; protected set; }
		public Int32 Quantity { get; protected set; }
		public String ApplicationUsername { get; protected set; }

		public void UpdateFromObject(System.Object data)
		{
			System.Collections.IDictionary keyValueData = (System.Collections.IDictionary)data;

			ProductIdentifier = keyValueData["productIdentifier"].ToString();
			Quantity = Int32.Parse(keyValueData["quantity"].ToString());
			ApplicationUsername = keyValueData["applicationUsername"].ToString();
		}
	}
}