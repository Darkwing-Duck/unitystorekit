using AOT;
using MiniJSON;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace UnityStoreKit
{
	public class StoreKitProvider
	{
		private static IStoreKitDelegate _storeKitDelegate;

		private delegate void ProductValidationSuccessDelegate(string json);
		private delegate void ProductValidationFailDelegate(string message);

		private delegate void PaymentTransactionsReceivedDelegate(string json);
		private delegate void ProductPaymentSuccessDelegate(string json);
		private delegate void ProductPaymentFailDelegate(string json);
		private delegate void StoreKitWarningDelegate(string message);

		[DllImport("__Internal")]
		private static extern void _Initialize(
												String applicationUsername,
												StoreKitWarningDelegate warningCallback,
												ProductValidationSuccessDelegate productValidationSuccessCallback,
												ProductValidationFailDelegate productValidationFailCallback,
												ProductPaymentSuccessDelegate productPaymentSuccessCallback,
												ProductPaymentFailDelegate productPaymentFailCallback,
												PaymentTransactionsReceivedDelegate paymentTransactionsReceived);

		public static void Initialize(String applicationUsername, IStoreKitDelegate storeKitDelegate)
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return;
			}

			_storeKitDelegate = storeKitDelegate;

			_Initialize(applicationUsername,
											OnStoreKitWarning,
											OnProductsValidationSuccess,
											OnProductsValidationFail,
											OnProductPaymentSuccess,
											OnProductPaymentFail,
											OnPaymentTransactionsReceived);
		}

		[DllImport("__Internal")]
		private static extern Boolean _CanMakePayments();

		public static Boolean CanMakePayments()
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return false;
			}

			return _CanMakePayments();
		}

		[DllImport("__Internal")]
		private static extern void _SetAutoFinishTransactions(Boolean value);

		public static void SetAutoFinishTransactions(Boolean value)
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return;
			}

			_SetAutoFinishTransactions(value);
		}

		[DllImport("__Internal")]
		private static extern void _GetPendingTransactions();

		public static void GetPendingTransactions()
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return;
			}

			_GetPendingTransactions();
		}

		[DllImport("__Internal")]
		private static extern void _ValidateProducts(String productIdentifiers);

		public static void ValidateProducts(String[] productIdentifiers)
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return;
			}

			_ValidateProducts(String.Join(",", productIdentifiers));
		}

		[DllImport("__Internal")]
		private static extern void _FinishTransaction(String transactionIdentifier);

		public static void FinishTransaction(String transactionIdentifier)
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return;
			}

			_FinishTransaction(transactionIdentifier);
		}

		[DllImport("__Internal")]
		private static extern void _PurchaseProduct(String productIdentifier, Int32 quantity);

		public static void PurchaseProduct(String productIdentifier, Int32 quantity)
		{
			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				return;
			}

			_PurchaseProduct(productIdentifier, quantity);
		}

		private static List<USKProduct> GetProductsFromJson(String json)
		{
			List<USKProduct> validProducts = new List<USKProduct>();

			System.Collections.IDictionary jsonDict = (System.Collections.IDictionary)MiniJSON.Json.Deserialize(json);
			System.Collections.IList rawProducts = (System.Collections.IList)jsonDict["validProducts"];

			foreach (System.Collections.IDictionary rawProduct in rawProducts)
			{
				USKProduct validProduct = new USKProduct();
				validProduct.UpdateFromObject(rawProduct);
				validProducts.Add(validProduct);
			}

			return validProducts;
		}

		private static List<USKPaymentTransaction> GetTransactionsFromJson(String json)
		{
			List<USKPaymentTransaction> pendingTransactions = new List<USKPaymentTransaction>();

			System.Collections.IDictionary jsonDict = (System.Collections.IDictionary)MiniJSON.Json.Deserialize(json);
			System.Collections.IList rawTransactions = (System.Collections.IList)jsonDict["transactions"];

			foreach (System.Collections.IDictionary rawTransaction in rawTransactions)
			{
				USKPaymentTransaction pendingTransaction = new USKPaymentTransaction();
				pendingTransaction.UpdateFromObject(rawTransaction);
				pendingTransactions.Add(pendingTransaction);
			}

			return pendingTransactions;
		}

		private static USKPaymentTransaction GetTransactionFromJson(String json)
		{
			System.Collections.IDictionary jsonDict = (System.Collections.IDictionary)MiniJSON.Json.Deserialize(json);
			System.Collections.IDictionary rawTransaction = (System.Collections.IDictionary)jsonDict["transaction"];

			USKPaymentTransaction transaction = new USKPaymentTransaction();
			transaction.UpdateFromObject(rawTransaction);

			return transaction;
		}

		[MonoPInvokeCallback(typeof(ProductValidationSuccessDelegate))]
		private static void OnProductsValidationSuccess(String json)
		{
			if (_storeKitDelegate == null)
			{
				return;
			}

			List<USKProduct> products = GetProductsFromJson(json);
			_storeKitDelegate.OnProductsValidationSuccess(products);
		}

		[MonoPInvokeCallback(typeof(ProductValidationFailDelegate))]
		private static void OnProductsValidationFail(String error)
		{
			if (_storeKitDelegate == null)
			{
				return;
			}

			_storeKitDelegate.OnProductsValidationFail(error);
		}

		[MonoPInvokeCallback(typeof(PaymentTransactionsReceivedDelegate))]
		private static void OnPaymentTransactionsReceived(String json)
		{
			if (_storeKitDelegate == null)
			{
				return;
			}

			List<USKPaymentTransaction> pendingTransactions = GetTransactionsFromJson(json);
			_storeKitDelegate.OnPendingTransactionsReceived(pendingTransactions);
		}

		[MonoPInvokeCallback(typeof(ProductPaymentSuccessDelegate))]
		private static void OnProductPaymentSuccess(String json)
		{
			if (_storeKitDelegate == null)
			{
				return;
			}

			USKPaymentTransaction transaction = GetTransactionFromJson(json);
			_storeKitDelegate.OnProductPaymentSuccess(transaction);
		}

		[MonoPInvokeCallback(typeof(ProductPaymentFailDelegate))]
		private static void OnProductPaymentFail(String json)
		{
			if (_storeKitDelegate == null)
			{
				return;
			}

			USKPaymentTransaction transaction = GetTransactionFromJson(json);
			_storeKitDelegate.OnProductPaymentFail(transaction);
		}

		[MonoPInvokeCallback(typeof(StoreKitWarningDelegate))]
		private static void OnStoreKitWarning(String message)
		{
			if (_storeKitDelegate == null)
			{
				return;
			}

			_storeKitDelegate.OnStoreKitWarning(message);
		}
	}
}