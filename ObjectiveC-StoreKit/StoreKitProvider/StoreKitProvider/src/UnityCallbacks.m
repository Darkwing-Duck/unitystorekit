//
//  UnityCallbacks.m
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/22/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (*Unity_OnStoreKitWarning)(const char *);

typedef void (*Unity_OnProductsValidationSuccess)(const char *);
typedef void (*Unity_OnProductsValidationFail)(const char *);

typedef void (*Unity_OnPaymentTransactionsReceived)(const char *);

typedef void (*Unity_OnProductPaymentSuccess)(const char *);
typedef void (*Unity_OnProductPaymentFail)(const char *);

struct UnityCallbacks
{
    Unity_OnStoreKitWarning OnStoreKitWarningCallback;
    Unity_OnProductsValidationSuccess OnProductsValidationSuccess;
    Unity_OnProductsValidationFail OnProductsValidationFail;
    Unity_OnPaymentTransactionsReceived OnPaymentTransactionsReceived;
    Unity_OnProductPaymentSuccess OnProductPaymentSuccess;
    Unity_OnProductPaymentFail OnProductPaymentFail;
} unityCallbacks;
