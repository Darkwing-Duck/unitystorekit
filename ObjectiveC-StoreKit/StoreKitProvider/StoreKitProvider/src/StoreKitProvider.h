//
//  StoreKitProvider.h
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/20/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

@import Foundation;
@import StoreKit;

#import "UnityCallbacks.m"

@interface StoreKitProvider : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
}

@property (readonly) bool IsStoreInitialized;
@property (readonly) bool IsStoreBusy;

- (void) initialize: (NSString *) applicationUsername;
- (bool) canMakePayments;
- (void) setAutoFinishTransactions: (BOOL) value;
- (NSString *) getPendingTransactions;

- (void) validateProductIdentifiers: (NSArray *)productIdentifiers;

- (void) purchaseProduct: (NSString *) productIdentifier quantity: (int) quantity;
- (void) finishTransaction: (NSString *) transactionIdentifier;

+ (StoreKitProvider *) sharedInstance;

@end
