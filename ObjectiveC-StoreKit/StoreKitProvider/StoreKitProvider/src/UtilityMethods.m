//
//  UtilityMethods.m
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/20/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString* CStringToNSString(const char* string)
{
    if (string)
    {
        return [NSString stringWithUTF8String: string];
    }
    
    return [NSString stringWithUTF8String: ""];
}

const char* NSStringToCStrings(NSString *string)
{
    if (string)
    {
        return [string UTF8String];
    }
    
    return "";
}