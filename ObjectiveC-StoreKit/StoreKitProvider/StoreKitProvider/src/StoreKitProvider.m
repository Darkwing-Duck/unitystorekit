//
//  StoreKitProvider.m
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/20/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

#import "StoreKitProvider.h"
#import "UtilityMethods.m"
#import "UnityCallbacks.m"
#import "StoreKitJsonSerializer.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation StoreKitProvider
{
    @private
    bool _isStoreInitialized;
    bool _isStoreBusy;
    bool _autoFinishTransactions;
    NSString *_applicationUsername;
}

@synthesize IsStoreInitialized = _isStoreInitialized;
@synthesize IsStoreBusy = _isStoreBusy;

- (void) initialize: (NSString *) applicationUsername
{
    if (_isStoreInitialized)
    {
        [self sendWarning: @"The store is already initialized!"];
        return;
    }
    
    _applicationUsername = applicationUsername;
    _isStoreBusy = false;
    _isStoreInitialized = true;
    _autoFinishTransactions = true;
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver: self];
}

- (void) setAutoFinishTransactions: (BOOL) value
{
    _autoFinishTransactions = value;
}

- (bool) canMakePayments
{
    return [SKPaymentQueue canMakePayments];
}

- (void) finishTransaction: (NSString *) transactionIdentifier
{
    SKPaymentTransaction *transaction = [self getTransaction: transactionIdentifier];
    
    if (transaction == nil)
    {
        NSString *error = [NSString stringWithFormat: @"Can't find pending transaction '%@'", transactionIdentifier];
        const char *convertedString = NSStringToCStrings(error);
        unityCallbacks.OnStoreKitWarningCallback(convertedString);
        return;
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (SKPaymentTransaction *) getTransaction: (NSString *)transactionIdentifier
{
    for (SKPaymentTransaction *transaction in [SKPaymentQueue defaultQueue].transactions)
    {
        if ([transaction.transactionIdentifier isEqualToString: transactionIdentifier])
        {
            return transaction;
        }
    }
    
    return nil;
}

- (NSString *) getPendingTransactions
{
    NSArray<SKPaymentTransaction*> *transactions = [SKPaymentQueue defaultQueue].transactions;
    NSString *result = [StoreKitJsonSerializer getTransactionsListJson: transactions];
    
    return result;
}

- (void) validateProductIdentifiers: (NSArray *)productIdentifiers
{
    if (!_isStoreInitialized)
    {
        [self sendWarning: @"You need to initialize the store before validating products!"];
        return;
    }
    
    if (_isStoreBusy)
    {
        [self sendWarning: @"Store is busy now. You need to handle when store complete current process!"];
        return;
    }
    
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray: productIdentifiers]];
    productsRequest.delegate = self;
    [productsRequest start];
    
    _isStoreBusy = true;
}

- (void)productsRequest: (SKProductsRequest *)request didReceiveResponse: (SKProductsResponse *)response
{
    _isStoreBusy = false;
    
    NSString *result = [StoreKitJsonSerializer getProductsListJson: response.products];
    const char *convertedString = NSStringToCStrings(result);
    
    unityCallbacks.OnProductsValidationSuccess(convertedString);
}

- (void)request: (SKRequest *)request didFailWithError: (NSError *)error
{
    const char *convertedString = NSStringToCStrings(error.localizedDescription);
    unityCallbacks.OnProductsValidationFail(convertedString);
}

- (void) purchaseProduct: (NSString *) productIdentifier quantity: (int) quantity
{
    if (!_isStoreInitialized)
    {
        [self sendWarning: @"You need to initialize the store before purchasing products!"];
        return;
    }
    
    SKMutablePayment *payment = [[SKMutablePayment alloc] init];
    
    if (_applicationUsername != nil)
    {
        payment.applicationUsername = [self hashedValueForApplicationUsername: _applicationUsername];
    }
    
    payment.productIdentifier = productIdentifier;
    payment.quantity = quantity;
    
    [[SKPaymentQueue defaultQueue] addPayment: payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStateDeferred:
                break;
            case SKPaymentTransactionStateFailed:
                [self onTransactionFailed: transaction];
                break;
            case SKPaymentTransactionStatePurchased:
                [self onTransactionSuccess: transaction];
                break;
            case SKPaymentTransactionStateRestored:
                break;
            default:
                [self sendWarning: [NSString stringWithFormat: @"Unexpected transaction state %@", @(transaction.transactionState)]];
                break;
        }
        
        if (_autoFinishTransactions)
        {
            [self finishTransaction: transaction.transactionIdentifier];
        }
    }
}

- (void) onTransactionSuccess: (SKPaymentTransaction *)transaction
{
    NSString *json = [StoreKitJsonSerializer getTransactionJson: transaction];
    const char *convertedString = NSStringToCStrings(json);
    unityCallbacks.OnProductPaymentSuccess(convertedString);
}

- (void) onTransactionFailed: (SKPaymentTransaction *)transaction
{
    NSString *json = [StoreKitJsonSerializer getTransactionJson: transaction];
    const char *convertedString = NSStringToCStrings(json);
    unityCallbacks.OnProductPaymentFail(convertedString);
}

- (void) sendWarning: (NSString *) message
{
    const char *convertedString = NSStringToCStrings(message);
    unityCallbacks.OnStoreKitWarningCallback(convertedString);
}

- (NSString *)hashedValueForApplicationUsername: (NSString*)applicationUsername
{
    const int HASH_SIZE = 32;
    unsigned char hashedChars[HASH_SIZE];
    const char *accountName = [applicationUsername UTF8String];
    size_t accountNameLen = strlen(accountName);
    
    // Confirm that the length of the user name is small enough
    // to be recast when calling the hash function.
    if (accountNameLen > UINT32_MAX) {
        NSLog(@"Account name too long to hash: %@", applicationUsername);
        return nil;
    }
    CC_SHA256(accountName, (CC_LONG)accountNameLen, hashedChars);
    
    // Convert the array of bytes into a string showing its hex representation.
    NSMutableString *userAccountHash = [[NSMutableString alloc] init];
    for (int i = 0; i < HASH_SIZE; i++) {
        // Add a dash every four bytes, for readability.
        if (i != 0 && i%4 == 0) {
            [userAccountHash appendString:@"-"];
        }
        [userAccountHash appendFormat:@"%02x", hashedChars[i]];
    }
    
    return userAccountHash;
}

#pragma mark - Singleton Instance
+ (StoreKitProvider *) sharedInstance
{
    static StoreKitProvider *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[StoreKitProvider alloc] init];
    });
    
    return _sharedInstance;
}

@end