//
//  UnityGate.m
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/20/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreKitProvider.h"
#import "UtilityMethods.m"
#import "UnityCallbacks.m"

void _Initialize(
                 const char *applicationUsername,
                 Unity_OnStoreKitWarning warningCallback,
                 Unity_OnProductsValidationSuccess productValidationSuccessCallback,
                 Unity_OnProductsValidationFail productValidationFailCallback,
                 Unity_OnProductPaymentSuccess productPaymentSuccessCallback,
                 Unity_OnProductPaymentFail productPaymentFailCallback,
                 Unity_OnPaymentTransactionsReceived paymentTransactionsReceived)
{
    unityCallbacks.OnStoreKitWarningCallback = warningCallback;
    unityCallbacks.OnProductsValidationSuccess = productValidationSuccessCallback;
    unityCallbacks.OnProductsValidationFail = productValidationFailCallback;
    unityCallbacks.OnProductPaymentSuccess = productPaymentSuccessCallback;
    unityCallbacks.OnProductPaymentFail = productPaymentFailCallback;
    unityCallbacks.OnPaymentTransactionsReceived = paymentTransactionsReceived;
    
    NSString *convertedString = CStringToNSString(applicationUsername);
    [[StoreKitProvider sharedInstance] initialize: convertedString];
}

bool _CanMakePayments()
{
    return [[StoreKitProvider sharedInstance] canMakePayments];
}

void _SetAutoFinishTransactions(BOOL value)
{
    return [[StoreKitProvider sharedInstance] setAutoFinishTransactions: value];
}

void _ValidateProducts(const char *productIdentifiers)
{
    NSString *convertedString = CStringToNSString(productIdentifiers);
    NSArray *productIdentifiersList = [convertedString componentsSeparatedByString: @","];
    [[StoreKitProvider sharedInstance] validateProductIdentifiers: productIdentifiersList];
}

void _GetPendingTransactions()
{
    NSString *transactionsJson = [[StoreKitProvider sharedInstance] getPendingTransactions];
    const char *convertedString = NSStringToCStrings(transactionsJson);
    
    unityCallbacks.OnPaymentTransactionsReceived(convertedString);
}

void _FinishTransaction(const char *transactionIdentifier)
{
    NSString *convertedString = CStringToNSString(transactionIdentifier);
    [[StoreKitProvider sharedInstance] finishTransaction: convertedString];
}

void _PurchaseProduct(const char *productIdentifier, int quantity)
{
    NSString *convertedProductId = CStringToNSString(productIdentifier);
    [[StoreKitProvider sharedInstance] purchaseProduct: convertedProductId quantity: quantity];
}