//
//  JsonConverter.m
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/21/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

#import "StoreKitJsonSerializer.h"
#import <StoreKit/StoreKit.h>
#import "../UnityCallbacks.m"

@implementation StoreKitJsonSerializer

+ (NSString *) convertToJsonString: (id) data
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: data options: 0 error: &error];
    
    if (!jsonData)
    {
        NSLog(@"ConvertToJsonString: error: %@", error.localizedDescription);
        return nil;
    }
    
    NSString *result = [[NSString alloc] initWithData: jsonData encoding: NSUTF8StringEncoding];
    return result;
}

+ (NSString *) getProductsListJson: (NSArray<SKProduct *> *) products
{
    NSMutableArray *jsonProducts = [NSMutableArray array];
    
    for (SKProduct *product in products)
    {
        [jsonProducts addObject: [StoreKitJsonSerializer getSKProductDict: product]];
    }
    
    NSMutableDictionary<NSString*, NSArray*> *resultDict = [NSMutableDictionary dictionary];
    [resultDict setObject: jsonProducts forKey: @"validProducts"];
    
    NSString *result = [StoreKitJsonSerializer convertToJsonString: resultDict];
    return result;
}

+ (NSString *) getTransactionsListJson: (NSArray<SKPaymentTransaction *> *) transactions;
{
    NSMutableArray *jsonTransactions = [NSMutableArray array];
    
    for (SKPaymentTransaction *transaction in transactions)
    {
        [jsonTransactions addObject: [StoreKitJsonSerializer getSKPaymentTransactionDict: transaction]];
    }
    
    NSMutableDictionary<NSString*, NSArray*> *resultDict = [NSMutableDictionary dictionary];
    [resultDict setObject: jsonTransactions forKey: @"transactions"];
    
    NSString *result = [StoreKitJsonSerializer convertToJsonString: resultDict];
    return result;
}

+ (NSString *) getTransactionJson: (SKPaymentTransaction *) transaction
{
    NSDictionary<NSString*, NSString*> *transactionDict = [StoreKitJsonSerializer getSKPaymentTransactionDict: transaction];
    
    NSMutableDictionary<NSString*, NSDictionary*> *resultDict = [NSMutableDictionary dictionary];
    [resultDict setObject: transactionDict forKey: @"transaction"];
    
    NSString *result = [StoreKitJsonSerializer convertToJsonString: resultDict];
    return result;
}

+ (NSDictionary<NSString*, NSString*> *) getSKPaymentTransactionDict: (SKPaymentTransaction *) transaction
{
    NSString *serializedState = [NSString stringWithFormat: @"%ld", (long)transaction.transactionState];
    
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
    [resultDict setObject: transaction.transactionIdentifier forKey: @"transactionIdentifier"];
    
    if (transaction.error)
    {
        [resultDict setObject: transaction.error.localizedDescription forKey: @"error"];
    }
    
    if (transaction.originalTransaction)
    {
        [resultDict setObject: [self getSKPaymentTransactionDict: transaction.originalTransaction] forKey: @"originalTransaction"];
    }
    
    [resultDict setObject: serializedState forKey: @"transactionState"];
    [resultDict setObject: [self getSKPaymentDict: transaction.payment] forKey: @"payment"];
    
    NSData *receipt = [self getTransactionReceipt: transaction];
    
    if (receipt)
    {
        [resultDict setObject: [receipt base64EncodedStringWithOptions: 0] forKey: @"transactionReceipt"];
    }
    
    return resultDict;
}

+ (NSData *) getTransactionReceipt: (SKPaymentTransaction *) transaction
{
    #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000 // iOS 7.0+ supported
        NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
        NSData *receipt = [NSData dataWithContentsOfURL: receiptURL];
    
        if (!receipt)
        {
            unityCallbacks.OnStoreKitWarningCallback("Transaction receipt wasn't loaded!");
        }
    
        return receipt;
    #else
        return transaction.transactionReceipt;
    #endif
}

+ (NSDictionary<NSString*, NSString*> *) getSKPaymentDict: (SKPayment *) payment
{
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
    [resultDict setObject: payment.productIdentifier forKey: @"productIdentifier"];
    [resultDict setObject: [NSString stringWithFormat: @"%i", (int)payment.quantity] forKey: @"quantity"];
    [resultDict setObject: payment.applicationUsername forKey: @"applicationUsername"];
    
    return resultDict;
}

+ (NSDictionary<NSString*, NSString*> *) getSKProductDict: (SKProduct *) product
{
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
    [resultDict setObject: product.productIdentifier forKey: @"productIdentifier"];
    [resultDict setObject: product.localizedTitle forKey: @"localizedTitle"];
    [resultDict setObject: product.localizedDescription forKey: @"localizedDescription"];
    [resultDict setObject: product.price forKey: @"price"];
    [resultDict setObject: [NSString stringWithFormat: @"%d", product.downloadable] forKey: @"isDownloadable"];
    [resultDict setObject: [self getFormattedPrice: product] forKey: @"formattedPrice"];
    [resultDict setObject: [self getCurrencyCode: product] forKey: @"currencyCode"];
    [resultDict setObject: [self getCurrencySymbol: product] forKey: @"currencySymbol"];
    [resultDict setObject: [self getLanguageCode: product] forKey: @"languageCode"];
    [resultDict setObject: [self getCountryCode: product] forKey: @"countryCode"];
    [resultDict setObject: [self getLocaleIdentifier: product] forKey: @"localeIdentifier"];

    return resultDict;
}

+ (NSString *) getCurrencyCode: (SKProduct *) product
{
    return [product.priceLocale objectForKey: NSLocaleCurrencyCode];
}

+ (NSString *) getCurrencySymbol: (SKProduct *) product
{
    return [product.priceLocale objectForKey: NSLocaleCurrencySymbol];
}

+ (NSString *) getLanguageCode: (SKProduct *) product
{
    return [product.priceLocale objectForKey: NSLocaleLanguageCode];
}

+ (NSString *) getCountryCode: (SKProduct *) product
{
    return [product.priceLocale objectForKey: NSLocaleCountryCode];
}

+ (NSString *) getLocaleIdentifier: (SKProduct *) product
{
    return [product.priceLocale objectForKey: NSLocaleIdentifier];
}

+ (NSString *) getFormattedPrice: (SKProduct *) product
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    NSString *formattedPrice = [numberFormatter stringFromNumber:product.price];
    
    return formattedPrice;
}

@end
