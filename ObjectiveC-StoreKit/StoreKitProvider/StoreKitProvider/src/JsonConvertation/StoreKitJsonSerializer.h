//
//  JsonConverter.h
//  StoreKitProvider
//
//  Created by Darkwing Duck on 10/21/15.
//  Copyright © 2015 Persha Studia. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import <Foundation/Foundation.h>

@interface StoreKitJsonSerializer : NSObject

+ (NSString *) convertToJsonString: (id) data;
+ (NSString *) getProductsListJson: (NSArray<SKProduct *> *) products;
+ (NSString *) getTransactionJson: (SKPaymentTransaction *) transaction;
+ (NSString *) getTransactionsListJson: (NSArray<SKPaymentTransaction *> *) transactions;

@end
